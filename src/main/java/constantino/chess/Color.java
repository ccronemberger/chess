package constantino.chess;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * The color also defines the direction of the movement.
 */
@AllArgsConstructor
@Getter
public enum Color {
    BLACK(-1, 7), WHITE(1, 0);

    /**
     * Direction the pawns can move
     */
    private int direction;

    /**
     * Row where the pieces will be placed initially. Pawns are placed at initialRow + direction
     */
    private int initialRow;

    public Color opponent() {
        return this == BLACK ? WHITE : BLACK;
    }
}
