package constantino.chess;

import constantino.chess.exceptions.InvalidaMoveException;
import constantino.chess.pieces.*;
import lombok.Getter;
import lombok.Setter;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * We need to save the board state before processing a move because if after completing the move we found that
 * the king is endangered we will need to revert the move and raise an exception.
 */
public class ChessboardState {

    private static final Class[] PROMOTION_OPTIONS = { Queen.class, Rook.class, Bishop.class, Knight.class };

    private static final Class[] INITIAL_ORDER = { Rook.class, Knight.class, Bishop.class, Queen.class, King.class,
            Bishop.class, Knight.class, Rook.class};

    public static final int NUMBER_OF_ROWS = 8;
    public static final int NUMBER_OF_COLUMNS = 8;

    /**
     * A null value means an empty cell
     */
    private Piece[][] rows;

    @Getter
    private Map<Color, Set<Piece>> removedPieces = new EnumMap<>(Color.class);

    /**
     * This is used to check if any piece of the opposite color can menace the king
     */
    @Getter
    private Map<Color, Set<Piece>> activePieces = new EnumMap<>(Color.class);

    /**
     * Used to control the turns
     */
    private Piece lastMoved;

    /**
     * A pawn will check the en passant candidate when validating its move
     */
    @Getter
    @Setter
    private Pawn enPassantCandidate;

    /**
     * This is used to check if any piece of the opponent can threaten the king.
     */
    private Map<Color, King> kings = new EnumMap<>(Color.class);

    private UserInputReader userInputReader;

    private List<String> messagesToUser = new ArrayList<>();

    private ChessboardState(ChessboardState source) {
        // copy all active pieces...
        activePieces.put(Color.BLACK, source.activePieces.get(Color.BLACK).stream().
                map(p -> p.getClone(this)).collect(Collectors.toSet()));
        activePieces.put(Color.WHITE, source.activePieces.get(Color.WHITE).stream().
                map(p -> p.getClone(this)).collect(Collectors.toSet()));

        // ...and all removed ones
        removedPieces.put(Color.BLACK, source.removedPieces.get(Color.BLACK).stream().
                map(p -> p.getClone(this)).collect(Collectors.toSet()));
        removedPieces.put(Color.WHITE, source.removedPieces.get(Color.WHITE).stream().
                map(p -> p.getClone(this)).collect(Collectors.toSet()));

        createRows();

        kings.put(Color.BLACK, (King) getPieceAt(source.kings.get(Color.BLACK).getPosition()));
        kings.put(Color.WHITE, (King) getPieceAt(source.kings.get(Color.WHITE).getPosition()));

        if (source.lastMoved != null) {
            lastMoved = getPieceAt(source.lastMoved.getPosition());
        }
        if (source.enPassantCandidate != null) {
            enPassantCandidate = (Pawn) getPieceAt(source.enPassantCandidate.getPosition());
        }

        userInputReader = source.userInputReader;

        messagesToUser = source.messagesToUser;
    }

    public ChessboardState(UserInputReader userInputReader) {
        this.userInputReader = userInputReader;
        createRows();

        removedPieces.put(Color.BLACK, new HashSet<>());
        removedPieces.put(Color.WHITE, new HashSet<>());

        activePieces = new EnumMap<>(Color.class);
        activePieces.put(Color.BLACK, new HashSet<>());
        activePieces.put(Color.WHITE, new HashSet<>());

        initialPositions(Color.WHITE);
        initialPositions(Color.BLACK);
    }

    private void createRows() {
        rows = new Piece[NUMBER_OF_ROWS][NUMBER_OF_COLUMNS];
        for (int i = 0; i < NUMBER_OF_ROWS; i++) {
            rows[i] = new Piece[NUMBER_OF_COLUMNS];
        }
        if (!activePieces.isEmpty()) {
            activePieces.get(Color.BLACK).forEach(p -> {
                Position position = p.getPosition();
                rows[position.getY()][position.getX()] = p;
            });
            activePieces.get(Color.WHITE).forEach(p -> {
                Position position = p.getPosition();
                rows[position.getY()][position.getX()] = p;
            });
        }
    }

    private void initialPositions(Color color) {
        assert NUMBER_OF_COLUMNS == INITIAL_ORDER.length;
        Position basePosition = new Position();
        Piece[] mainRow = rows[color.getInitialRow()];
        basePosition.setY(color.getInitialRow());
        Position basePawnPosition = new Position();
        basePawnPosition.setY(color.getInitialRow() + color.getDirection());
        Piece[] pawnsRow = rows[color.getInitialRow() + color.getDirection()];
        for (int x = 0; x < NUMBER_OF_COLUMNS; x++) {
            Position position = new Position(basePosition);
            position.setX(x);
            mainRow[x] = createInstance(INITIAL_ORDER[x], color, position);
            activePieces.get(color).add(mainRow[x]);
            position = new Position(basePawnPosition);
            position.setX(x);
            pawnsRow[x] = new Pawn(color, this, position);
            activePieces.get(color).add(pawnsRow[x]);
        }
    }

    private Piece createInstance(Class<? extends Piece> pieceClass, Color color, Position position) {
        try {
            Piece piece = (Piece) pieceClass.getConstructors()[0].newInstance(color, this, position);
            if (pieceClass == King.class) {
                kings.put(color, (King) piece);
            }
            return piece;
        } catch (Exception e) {
            throw new IllegalStateException("error creating instance of " + pieceClass, e);
        }
    }

    private boolean iterateOverPath(Position initialPosition, Position finalPosition, Predicate<Position> function)
            throws InvalidaMoveException {
        int deltaX = finalPosition.getX() - initialPosition.getX();
        int deltaY = finalPosition.getY() - initialPosition.getY();
        int absX = Math.abs(deltaX);
        int absY = Math.abs(deltaY);
        if (absX != 0 && absY != 0 && absX != absY) {
            throw new InvalidaMoveException("This move can only be horizontal, vertical or diagonal");
        }
        if (absX != 0) {
            deltaX = deltaX / absX;
        }
        if (absY != 0) {
            deltaY = deltaY / absY;
        }

        Position position = new Position(initialPosition);
        position.moveBy(deltaX, deltaY);

        while (!position.equals(finalPosition)) {
            if (!function.test(position)) {
                return false;
            }
            position.moveBy(deltaX, deltaY);
        }
        return true;
    }

    /**
     * This is used by pieces that can't leap over other pieces. It also checks if the move is vertical, horizontal or
     * diagonal.
     * @param initialPosition initial position of the path
     * @param finalPosition   final position of the path
     * @return true if none of the cells in the path are occupied
     */
    public boolean isPathFree(Position initialPosition, Position finalPosition) throws InvalidaMoveException {
        return iterateOverPath(initialPosition, finalPosition, position -> getPieceAt(position) == null);
    }

    /**
     * This method check if any cells in the path of a castling are under attack. If this is true then castling can't
     * be performed.
     * @param initialPosition current position of the king
     * @param finalPosition final position of the king
     * @param color the color making the move
     */
    public boolean isPathUnderAttack(Position initialPosition, Position finalPosition, Color color) throws InvalidaMoveException {
        return iterateOverPath(initialPosition, finalPosition, position -> isPositionUnderAttack(color, position));
    }

    public Piece getPieceAt(Position position) {
        return rows[position.getY()][position.getX()];
    }

    public Piece getPieceAt(int x, int y) {
        return rows[y][x];
    }

    /**
     * Set the piece to the given position or clear what is in that position
     * @param piece a piece of null
     * @param position the position where to set the piece or to be cleared
     */
    public void setPieceAt(Piece piece, Position position) {
        rows[position.getY()][position.getX()] = piece;
        if (piece != null) {
            piece.movedTo(position);
        }
    }

    ChessboardState createCopy() {
        return new ChessboardState(this);
    }

    public void removePiece(Piece toBeRemovedPiece, boolean showInfo) {
        removedPieces.get(toBeRemovedPiece.getColor()).add(toBeRemovedPiece);
        activePieces.get(toBeRemovedPiece.getColor()).remove(toBeRemovedPiece);
        // in the case of en passant the captured piece is in a different position compared to the final position
        // of the move
        setPieceAt(null, toBeRemovedPiece.getPosition());
        Position position = toBeRemovedPiece.getPosition();
        toBeRemovedPiece.setPosition(null);

        if (showInfo) {
            messagesToUser.add(toBeRemovedPiece.getColor() + " " + toBeRemovedPiece.getDescription() +
                    " captured in position " + position);
        }
    }

    /**
     * When a pawn reaches the other side of the board it can be promoted to queen, rook, bishop, or knight.
     * @param piece only pawns can be promoted
     */
    private void handlePromotion(Piece piece) {
        if (piece instanceof Pawn) {
            Color color = piece.getColor();
            if (piece.getPosition().getY() == color.getInitialRow() + color.getDirection() * (NUMBER_OF_ROWS - 1)) {
                // in the case of promotion we can print things here because at this stage we know the move is valid
                System.out.println("To which piece do you want to promote the " + color + " " + piece.getDescription() +
                        " at position " + piece.getPosition() + "?");
                StringBuilder sb = new StringBuilder("Valid options: ");
                for (Class cls : PROMOTION_OPTIONS) {
                    sb.append(cls.getSimpleName()).append(", ");
                }
                sb.deleteCharAt(sb.length() - 2);
                sb.append("\n> ");
                Class<? extends Piece> selectedClass = getSelectedType(sb);
                Position position = piece.getPosition();
                removePiece(piece, false);
                Piece newPiece = createInstance(selectedClass, color, piece.getPosition());
                setPieceAt(newPiece, position);
                activePieces.get(color).add(newPiece);
            }
        }
    }

    private Class<? extends Piece> getSelectedType(StringBuilder sb) {
        Class selectedClass = null;
        while (selectedClass == null) {
            System.out.print(sb.toString());
            String selection = userInputReader.getUserInput();
            selection = selection.trim();
            for (Class cls : PROMOTION_OPTIONS) {
                if (cls.getSimpleName().equalsIgnoreCase(selection)) {
                    selectedClass = cls;
                }
            }
        }
        return selectedClass;
    }

    private void checkTurn(Piece piece) throws InvalidaMoveException {
        if (lastMoved == null) {
            if (piece.getColor() != Color.WHITE) {
                throw new InvalidaMoveException("the whites always move first");
            }
        } else {
            if (piece.getColor() == lastMoved.getColor()) {
                throw new InvalidaMoveException("the last move was already made by " + piece.getColor());
            }
        }
    }

    private void isValidPosition(Position position) throws InvalidaMoveException {
        if (position.getX() < 0 || position.getX() >= NUMBER_OF_COLUMNS) {
            throw new InvalidaMoveException("column " + position.getX() + " is invalid");
        }
        if (position.getY() < 0 || position.getY() >= NUMBER_OF_ROWS) {
            throw new InvalidaMoveException("row " + position.getY() + " is invalid");
        }
    }

    public void move(Position initialPosition, Position finalPosition) throws InvalidaMoveException {
        isValidPosition(initialPosition);
        isValidPosition(finalPosition);
        if (initialPosition.equals(finalPosition)) {
            throw new InvalidaMoveException("moving a piece to its current position is not valid");
        }

        Piece piece = getPieceAt(initialPosition);
        if (piece == null) {
            throw new InvalidaMoveException("there is no piece in position " + initialPosition);
        }

        checkTurn(piece);

        Piece toBeRemovedPiece = piece.prepareMove(finalPosition);

        if (toBeRemovedPiece != null && toBeRemovedPiece.getColor() == piece.getColor()) {
            throw new InvalidaMoveException("you can't capture your own piece at position " + finalPosition);
        }

        messagesToUser.add(piece.getColor() + " " + piece.getDescription() + " moved from " + initialPosition + " to " + finalPosition);
        piece.performMove(finalPosition);
        setPieceAt(null, initialPosition);
        if (toBeRemovedPiece != null) {
            removePiece(toBeRemovedPiece, true);
        }
        setPieceAt(piece, finalPosition);
        lastMoved = piece;

        // after a move of the other color we clear the en passant candidate because if capturing this way it has
        // to be in the next move
        if (enPassantCandidate != null && enPassantCandidate.getColor() != piece.getColor()) {
            enPassantCandidate = null;
        }

        // Check if the king would be endangered
        if (isKingEndangered(piece.getColor())) {
            throw new InvalidaMoveException("This move can't be performed otherwise the " + piece.getColor() +
                    " king would be or already is endangered");
        }

        // promotion will never endanger the king, so we can call it at the end
        handlePromotion(piece);
    }

    /**
     * Checks if the given position is under attack by the opposite color.
     * @param color the color of that is performing the current move
     * @param position position to check
     * @return true if the position is under attack
     */
    private boolean isPositionUnderAttack(Color color, Position position) {
        Color opponentColor = color.opponent();
        Set<Piece> opponentPieces = new HashSet<>(activePieces.get(opponentColor));
        boolean underAttack = false;
        for (Piece piece : opponentPieces) {
            try {
                piece.prepareMove(position);
                underAttack = true;
            } catch (InvalidaMoveException ex) {
                // no need to log anything here
            }
        }
        return underAttack;
    }

    /**
     * Need to save the state before the move. Then we make the move and check if the king would be endangered in
     * which case the move is invalid. In this case we must rollback to the initial state.
     * The check would not be valid if we don't complete the move we are checking.
     */
    boolean isKingEndangered(Color color) {
        return isPositionUnderAttack(color, kings.get(color).getPosition());
    }

    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(" >----0-----1-----2-----3-----4-----5-----6-----7----<\n");
        for (int i = NUMBER_OF_ROWS -1; i >= 0 ; i--) {
            sb.append(i).append("> | ");
            for (int j = 0; j < NUMBER_OF_COLUMNS; j++) {
                Piece piece = rows[i][j];
                if (piece == null) {
                    sb.append("   ");
                } else {
                    sb.append(piece.toString());
                }
                sb.append(" | ");
            }
            sb.append("<\n");
            sb.append(" >---------------------------------------------------<\n");
        }
        return sb.toString();
    }

    /**
     * Used in unit tests to make it easier to create some scenarios. The king is never removed.
     */
    void removeAllBut(Color color, Position ... positions) {
        Set<Piece> pieces = new HashSet<>(activePieces.get(color));
        for (Position p : positions) {
            pieces.remove(getPieceAt(p));
        }
        pieces.forEach(p -> {
            if (!(p instanceof King)) {
                removePiece(p, false);
            }
        });
    }

    /**
     * Messages are printed only at the end if the move is really valid after all checks
     */
    void printMessagesToUser() {
        messagesToUser.forEach(message -> System.out.println("-> " +message));
        messagesToUser.clear();
    }

    public Color getNextToMove() {
        if (lastMoved == null) {
            return Color.WHITE;
        }
        return lastMoved.getColor().opponent();
    }
}
