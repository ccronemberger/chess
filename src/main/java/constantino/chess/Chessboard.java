package constantino.chess;

import constantino.chess.exceptions.CheckmateException;
import constantino.chess.exceptions.InvalidaMoveException;
import constantino.chess.pieces.Piece;

import java.util.Set;

import static constantino.chess.ChessboardState.NUMBER_OF_COLUMNS;
import static constantino.chess.ChessboardState.NUMBER_OF_ROWS;

public class Chessboard {

    // this is protected to allow test classes to use it
    protected ChessboardState state;

    public Chessboard(UserInputReader userInputReader) {
        state = new ChessboardState(userInputReader);
    }

    public Chessboard() {
        this(null);
    }

    public void move(Position initialPosition, Position finalPosition) throws InvalidaMoveException {
        // Need to create a copy of the state because the check to see if the king would be in check can only
        // be done after the move was applied. With a copy, if the move is not valid, we don't update the state and
        // the new invalid state is discarded.
        ChessboardState newState = state.createCopy();
        try {
            newState.move(initialPosition, finalPosition);
            state = newState;
            state.printMessagesToUser();
        } catch (InvalidaMoveException ex) {
            throw ex;
        }

        Color nextMoveColor = state.getNextToMove();
        if (state.isKingEndangered(nextMoveColor)) {
            System.out.println(nextMoveColor + " king is in check");
            if (isCheckmate(nextMoveColor)) {
                throw new CheckmateException("Checkmate, the " + nextMoveColor.opponent() + " won");
            }
        } else {
            // TODO: check if next player has a legal move and still not in check
            // TODO: If during the previous 50 moves no pawn has been moved and no capture has been made, either player can claim a draw
            // TODO: there are other complex end conditions
        }
    }

    /**
     * The player whose turn it is to move is in check and has no legal move to escape check.
     * This method is called only when there is a check, so we only need to verify that there is no legal move left.
     * @param nextMoveColor
     * @return true if we identified a checkmate
     */
    public boolean isCheckmate(Color nextMoveColor) {
        Set<Piece> set = state.getActivePieces().get(nextMoveColor);
        for (Piece piece : set) {
            if (hasValidMove(piece)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Brute force implementation: go over all cells and try to find a valid move.
     */
    private boolean hasValidMove(Piece piece) {
        for (int x = 0; x < NUMBER_OF_COLUMNS; x++) {
            for (int y = 0; y < NUMBER_OF_ROWS; y++) {
                Position position = new Position(x,y);
                if (!position.equals(piece.getPosition())) {
                    try {
                        checkMove(piece.getPosition(), position);
                        System.out.println("valid move " + piece.getPosition() + " - " + position);
                        return true;
                    } catch (InvalidaMoveException e) {
                    }
                }
            }
        }
        return false;
    }

    private void checkMove(Position initialPosition, Position finalPosition) throws InvalidaMoveException {
        ChessboardState newState = state.createCopy();
        newState.move(initialPosition, finalPosition);
    }

    public void printBoard() {
        System.out.println(state.toString());
    }

    public String getTurn() {
        return state.getNextToMove().toString();
    }
}
