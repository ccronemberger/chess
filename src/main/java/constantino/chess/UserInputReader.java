package constantino.chess;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UserInputReader {
    private BufferedReader reader;

    public UserInputReader() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    public String getUserInput() {
        try {
            return reader.readLine();
        } catch (IOException e) {
            throw new RuntimeException("error reading user input", e);
        }
    }
}
