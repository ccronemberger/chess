package constantino.chess.exceptions;

public class CheckmateException extends RuntimeException {
    public CheckmateException(String message) {
        super(message);
    }
}
