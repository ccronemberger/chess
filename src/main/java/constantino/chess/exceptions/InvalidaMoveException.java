package constantino.chess.exceptions;

public class InvalidaMoveException extends Exception {
    public InvalidaMoveException(String message) {
        super(message);
    }
}
