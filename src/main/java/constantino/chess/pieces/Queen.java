package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;

public class Queen extends NonLeapingPiece {
    public Queen(Color color, ChessboardState board, Position position) {
        super(color, board, position);
    }

    @Override
    public Piece prepareMove(Position finalPos) throws InvalidaMoveException {
        super.prepareMove(finalPos);
        return getBoard().getPieceAt(finalPos);
    }
}
