package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;

public class Knight extends Piece {
    public Knight(Color color, ChessboardState board, Position position) {
        super(color, board, position);
    }

    @Override
    public Piece prepareMove(Position finalPosition) throws InvalidaMoveException {
        Position initialPosition = getPosition();
        int deltaX = Math.abs(initialPosition.getX() - finalPosition.getX());
        int deltaY = Math.abs(initialPosition.getY() - finalPosition.getY());
        if (!((deltaX == 1 && deltaY == 2) || (deltaX == 2 && deltaY == 1))) {
            throw new InvalidaMoveException("invalid move for the " + getDescription());
        }
        return getBoard().getPieceAt(finalPosition);
    }
}
