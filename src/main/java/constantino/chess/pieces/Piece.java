package constantino.chess.pieces;

import constantino.chess.Chessboard;
import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;
import lombok.Getter;
import lombok.Setter;

/**
 * Base class for all pieces
 */
@Getter
public abstract class Piece implements Cloneable {
    private Color color;
    private ChessboardState board;
    private String type;
    private boolean firstMove = true;

    /**
     * The position is used when we go through all pieces of the opponent to see if they can capture the king when
     * validating a move.
     * If after a move the king can be captured then that move is invalid.
     */
    @Setter
    private Position position;

    public Piece(Color color, ChessboardState board, Position position) {
        this.color = color;
        this.board = board;
        type = color.toString().substring(0,1) + getClass().getSimpleName().substring(0,2);
        this.position = position;
    }

    public void movedTo(Position position) {
        firstMove = false;
        this.position = position;
    }

    /**
     * Throws an exception if the move is not valid.
     * It does not check other conditions that are checked by the {@link Chessboard}, for example if the king would
     * be endangered.
     * @param finalPosition   target position of the piece
     * @return the piece that would be captured if any
     * @throws InvalidaMoveException if this is an invalid move
     */
    public abstract Piece prepareMove(Position finalPosition) throws InvalidaMoveException;

    /**
     * Only the king will override this method to perform castling after it is validated by the prepareMove call.
     */
    public void performMove(Position finalPosition) {}

    public String toString() {
        return type;
    }

    public String getDescription() {
        return getClass().getSimpleName();
    }

    public Piece getClone(ChessboardState board) {
        try {
            Piece newPiece = (Piece) clone();
            newPiece.board = board;
            if (position != null) {
                newPiece.position = new Position(position);
            }
            return newPiece;
        } catch (CloneNotSupportedException e) {
            throw new RuntimeException("error cloning", e);
        }
    }
}
