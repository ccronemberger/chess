package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;

public class Rook extends NonLeapingPiece {
    public Rook(Color color, ChessboardState board, Position position) {
        super(color, board, position);
    }

    @Override
    public Piece prepareMove(Position finalPosition) throws InvalidaMoveException {
        super.prepareMove(finalPosition);
        Position initialPosition = getPosition();
        int deltaX = Math.abs(initialPosition.getX() - finalPosition.getX());
        int deltaY = Math.abs(initialPosition.getY() - finalPosition.getY());
        if (deltaX != 0 && deltaY != 0) {
            throw new InvalidaMoveException("the " + getDescription() + " can only move vertically or horizontally");
        }
        return getBoard().getPieceAt(finalPosition);
    }
}
