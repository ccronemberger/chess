package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;

public class Pawn extends NonLeapingPiece {
    public Pawn(Color color, ChessboardState board, Position position) {
        super(color, board, position);
    }

    @Override
    public Piece prepareMove(Position finalPos) throws InvalidaMoveException {
        super.prepareMove(finalPos); // leap checking

        Position initialPos = getPosition();
        int deltaX = Math.abs(initialPos.getX() - finalPos.getX());
        int deltaY = finalPos.getY() - initialPos.getY();

        if (deltaX == 0) {
            // no capture is allowed on a straight move
            if (getBoard().getPieceAt(finalPos) != null) {
                throw new InvalidaMoveException("the " + getDescription() + " can't capture on a straight move");
            }
            int maxDeltaY = isFirstMove() ? 2 : 1;
            deltaY = deltaY * getColor().getDirection();
            if (deltaY > maxDeltaY) {
                throw new InvalidaMoveException("the " + getDescription() + " can move two squares in the first move or a single one afterwards");
            }
            if (deltaY < 0) {
                throw new InvalidaMoveException("the " + getDescription() + " can't move backwards");
            }
            if (deltaY == 2) {
                // en passant candidate
                getBoard().setEnPassantCandidate(this);
            }
            return null;
        } else {
            if (deltaX == 1 && deltaY == getColor().getDirection()) {

                // handle en passant
                Pawn enPassantCandidate = getBoard().getEnPassantCandidate();
                if (enPassantCandidate != null) {
                    // need a new instance to do not change the position of the candidate
                    Position position = new Position(enPassantCandidate.getPosition());
                    position.moveBy(0, getColor().getDirection());
                    if (finalPos.equals(position)) {
                        return enPassantCandidate;
                    }
                }

                // diagonal move is allowed only when capturing
                Piece piece = getBoard().getPieceAt(finalPos);
                if (piece == null) {
                    throw new InvalidaMoveException("the " + getDescription() + " can move diagonally only when capturing");
                }
                return piece;
            }
            throw new InvalidaMoveException("invalid move for the " + getDescription());
        }
    }
}
