package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;

public class NonLeapingPiece extends Piece {
    public NonLeapingPiece(Color color, ChessboardState board, Position position) {
        super(color, board, position);
    }

    @Override
    public Piece prepareMove(Position finalPos) throws InvalidaMoveException {
        if (!getBoard().isPathFree(getPosition(), finalPos)) {
            throw new InvalidaMoveException("a " + getDescription() + " can't leap over another piece");
        }
        return null;
    }
}
