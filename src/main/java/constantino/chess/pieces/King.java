package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;

/**
 * The King can't leap other pieces, but since it can only move to an adjacent cell there is not need to check
 * for leaping, so we don't need to extend {@link NonLeapingPiece}
 */
public class King extends Piece {

    public King(Color color, ChessboardState board, Position position) {
        super(color, board, position);
    }

    public Piece prepareMove(Position finalPosition) throws InvalidaMoveException {
        // check move, but do not perform castling yet
        return moveImpl(finalPosition, true);
    }

    /**
     * Handle castling in two stages: if checkOnly is true it will only validate, but will not perform the actual
     * castling.
     * @param finalPosition
     * @param checkOnly
     * @return
     * @throws InvalidaMoveException
     */
    private Piece moveImpl(Position finalPosition, boolean checkOnly) throws InvalidaMoveException {
        Position initialPosition = getPosition();
        int deltaX = initialPosition.getX() - finalPosition.getX();
        int deltaY = Math.abs(initialPosition.getY() - finalPosition.getY());

        // checking castling
        if (deltaY == 0 && isFirstMove()) {
            if (handleCastling(finalPosition, deltaX, 0, checkOnly)) return null;
            if (handleCastling(finalPosition, deltaX, 7, checkOnly)) return null;
        }

        deltaX = Math.abs(deltaX);
        // no need to check for no move because this is already checked by the @Chessboard
        if (deltaX > 1 || deltaY > 1) {
            throw new InvalidaMoveException("the " + getDescription() + " can only move to an adjacent position");
        }
        return getBoard().getPieceAt(finalPosition);
    }

    /**
     * We need a separate method to perform castling because when validating the checkmate we don't want to perform
     * castling if possible, we just want to check if the user has a valid move
     */
    @Override
    public void performMove(Position finalPosition) {
        try {
            // now we can perform castling
            moveImpl(finalPosition, false);
        } catch (InvalidaMoveException e) {
            // this will not happen here because prepareMove is called before this method and any exception that
            // would be thrown here will be thrown first there
        }
    }

    private boolean handleCastling(Position finalPosition, int deltaX, int column, boolean checkOnly) throws InvalidaMoveException {
        Position p = new Position(column, getColor().getInitialRow());
        Piece rook = getBoard().getPieceAt(p);
        int requiredDelta = column == 0 ? 1 : -1;
        if (rook instanceof Rook && rook.isFirstMove() && deltaX == (requiredDelta * 2)) {

            if (!getBoard().isPathFree(getPosition(), rook.getPosition())) {
                throw new InvalidaMoveException("Castling can't be performed because the path is not free");
            }

            if (getBoard().isPathUnderAttack(getPosition(), finalPosition, getColor())) {
                throw new InvalidaMoveException("Castling can't be performed because the path is under attack");
            }

            if (!checkOnly) {
                // move rook
                Position newRookPosition = new Position(finalPosition.getX() + requiredDelta, getColor().getInitialRow());
                getBoard().setPieceAt(null, rook.getPosition());
                getBoard().setPieceAt(rook, newRookPosition);
            }
            return true;
        }
        return false;
    }
}
