package constantino.chess;

import constantino.chess.exceptions.CheckmateException;
import constantino.chess.exceptions.InvalidaMoveException;

public class ChessGame {
    public static void main(String[] args) throws InvalidaMoveException {
        UserInputReader userInputReader = new UserInputReader();
        Chessboard chessboard = new Chessboard(userInputReader);
        System.out.println("Coordinates are x,y");
        System.out.println("Example: 1,1 - 1,2 will move the piece on 1,1 to 1,2");

        prepareGame(chessboard);

        while (true) {
            chessboard.printBoard();
            System.out.print(chessboard.getTurn() + " move: ");
            String input = userInputReader.getUserInput();
            try {
                performMove(chessboard, input);
            } catch (InvalidaMoveException ex) {
                System.out.println(ex.getMessage());
            } catch (CheckmateException ex) {
                System.out.println(ex.getMessage());
                break;
            }
        }
    }

    private static void prepareGame(Chessboard chessboard) throws InvalidaMoveException {
        Position queenPosition = new Position(3, 0);
        chessboard.state.removeAllBut(Color.WHITE, queenPosition);

        Position blackQueenPosition = new Position(3, 7);
        chessboard.state.removeAllBut(Color.BLACK, blackQueenPosition);
    }

    private static void performMove(Chessboard chessboard, String input) throws InvalidaMoveException {
        String[] parts = input.split("-");
        if (parts.length != 2) {
            System.out.println("wrong command format");
            return;
        }
        Position initialPosition;
        try {
            initialPosition = new Position(parts[0]);
        } catch (Exception ex) {
            System.out.println("invalid initial position format");
            return;
        }
        Position finalPosition;
        try {
            finalPosition = new Position(parts[1]);
        } catch (Exception ex) {
            System.out.println("invalid final position format");
            return;
        }
        chessboard.move(initialPosition, finalPosition);
    }
}
