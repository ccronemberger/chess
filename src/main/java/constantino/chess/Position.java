package constantino.chess;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@EqualsAndHashCode
public class Position {
    private int x;
    private int y;

    public Position(Position position) {
        x = position.x;
        y = position.y;
    }

    public Position(String string) {
        String[] parts = string.split(",");
        if (parts.length != 2) {
            throw new IllegalArgumentException();
        }
        x = Integer.parseInt(parts[0].trim());
        y = Integer.parseInt(parts[1].trim());
    }

    public void moveBy(int deltaX, int deltaY) {
        x += deltaX;
        y += deltaY;
    }

    public Position newPosition(int deltaX, int deltaY) {
        Position newPosition = new Position(this);
        newPosition.moveBy(deltaX, deltaY);
        return newPosition;
    }
}
