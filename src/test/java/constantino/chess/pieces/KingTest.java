package constantino.chess.pieces;

import constantino.chess.ChessboardState;
import constantino.chess.Color;
import constantino.chess.Position;
import constantino.chess.exceptions.InvalidaMoveException;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;

class KingTest {

    @Test
    public void testKingMoveOk() throws InvalidaMoveException {
        ChessboardState chessboard = new ChessboardState(null);
        Position position = new Position(4,4);
        King king = new King(Color.BLACK, chessboard, position);
        for (int x = 3; x <= 5; x++) {
            for (int y = 3; y <= 5; y++) {
                Position finalPosition = new Position(x,y);
                king.prepareMove(finalPosition);
            }
        }
        for (int x = -1; x <= 1; x++) {
            for (int y = -1; y <= 1; y++) {
                if (!(x == 0 && y == 0)) {
                    int deltaX = 2 * x;
                    int deltaY = 2 * y;
                    assertThrows(InvalidaMoveException.class,
                            () -> testInvalidMove(position, king, deltaX, deltaY));
                }
            }
        }
    }

    private void testInvalidMove(Position position, King king, int x, int y) throws InvalidaMoveException {
        Position finalPosition = new Position(position);
        finalPosition.moveBy(x,y);
        king.prepareMove(finalPosition);
    }
    @Test
    public void castlingTest() throws InvalidaMoveException {
        castlingTest(Color.BLACK, 0);
        castlingTest(Color.BLACK, 7);
        castlingTest(Color.WHITE ,0);
        castlingTest(Color.WHITE ,7);
    }

    public void castlingTest(Color color, int column) throws InvalidaMoveException {
        ChessboardState chessboard = new ChessboardState(null);
        if (color == Color.BLACK) {
            chessboard.move(new Position(0, 1), new Position(0, 2));
        }
        Piece king = chessboard.getPieceAt(new Position(4, color.getInitialRow()));
        Piece rook = chessboard.getPieceAt(new Position(column, color.getInitialRow()));
        Set<Piece> set = new HashSet<>(chessboard.getActivePieces().get(color));
        set.remove(king);
        set.remove(rook);
        set.forEach(p -> chessboard.removePiece(p, false));
        Position p = new Position(king.getPosition());
        int deltaX = column == 0 ? -1 : 1;
        p.moveBy(deltaX * 2, 0);
        chessboard.move(king.getPosition(), p);
        assertNull(chessboard.getPieceAt(new Position(column, color.getInitialRow())));
        p.moveBy(-deltaX, 0);
        Piece piece = chessboard.getPieceAt(p);
        assertSame(rook, piece);
    }

    @Test
    public void invalidCastlingTest() throws InvalidaMoveException {
        Color color = Color.WHITE;
        int column = 0;
        ChessboardState chessboard = new ChessboardState(null);
        Piece king = chessboard.getPieceAt(new Position(4, color.getInitialRow()));
        Piece rook = chessboard.getPieceAt(new Position(column, color.getInitialRow()));
        Piece bishop = chessboard.getPieceAt(new Position(2, color.getInitialRow()));
        Set<Piece> set = new HashSet<>(chessboard.getActivePieces().get(color));
        set.remove(king);
        set.remove(rook);
        set.remove(bishop);
        set.forEach(p -> chessboard.removePiece(p, false));

        Position p = new Position(king.getPosition());
        int deltaX = column == 0 ? -1 : 1;
        p.moveBy(deltaX * 2, 0);
        // castling can't be performed because the bishop is in the middle

        try {
            chessboard.move(king.getPosition(), p);
            fail("exception expected");
        } catch (InvalidaMoveException ex) {
            assertEquals("Castling can't be performed because the path is not free", ex.getMessage());
        }
    }
}