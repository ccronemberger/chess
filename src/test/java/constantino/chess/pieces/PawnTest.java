package constantino.chess.pieces;

import constantino.chess.*;
import constantino.chess.exceptions.InvalidaMoveException;
import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class PawnTest {

    @Test
    public void enPassantTest() throws Exception {
        ChessboardState chessboard = new ChessboardState(null);
        chessboard.move(new Position(3, 1), new Position(3, 3)); // W
        assertNotNull(chessboard.getEnPassantCandidate());

        chessboard.move(new Position(1, 6), new Position(1, 5)); // B
        assertNull(chessboard.getEnPassantCandidate());

        chessboard.move(new Position(3, 3), new Position(3, 4)); // W

        // en passant candidate
        chessboard.move(new Position(4, 6), new Position(4, 4)); // B

        // en passant capture
        chessboard.move(new Position(3, 4), new Position(4, 5)); // W
        assertNull(chessboard.getPieceAt(new Position(4,4)));
    }

    @Test
    public void falseEnPassantTest() throws Exception {
        ChessboardState chessboard = new ChessboardState(null);
        chessboard.move(new Position(3, 1), new Position(3, 3)); // W

        chessboard.move(new Position(1, 6), new Position(1, 4)); // B

        chessboard.move(new Position(3, 3), new Position(3, 4)); // W

        chessboard.move(new Position(4, 6), new Position(4, 4)); // B
        assertNotNull(chessboard.getEnPassantCandidate());

        chessboard.move(new Position(2, 1), new Position(2, 2)); // W
        assertNull(chessboard.getEnPassantCandidate());

        chessboard.move(new Position(2, 6), new Position(2, 5)); // B

        try {
            chessboard.move(new Position(3, 4), new Position(4, 5)); // W
            fail("exception expected");
        } catch (InvalidaMoveException ex) {
            assertEquals("the Pawn can move diagonally only when capturing", ex.getMessage());
        }
    }

    @Test
    public void promotionTest() throws InvalidaMoveException {
        UserInputReader userInputReader = mock(UserInputReader.class);
        when(userInputReader.getUserInput()).thenReturn("Queen");
        ChessboardState chessboard = new ChessboardState(userInputReader);

        Set<Piece> set = new HashSet<>(chessboard.getActivePieces().get(Color.WHITE));
        Piece whitePiece = chessboard.getPieceAt(new Position(2,1));
        Piece whiteKing = chessboard.getPieceAt(4, 0);
        set.remove(whitePiece);
        set.remove(whiteKing); // king can't be removed otherwise the check around it at the end will fail
        set.forEach(p -> chessboard.removePiece(p, false));
        chessboard.move(whiteKing.getPosition(), whiteKing.getPosition().newPosition(0, 1));

        set = new HashSet<>(chessboard.getActivePieces().get(Color.BLACK));
        Piece blackPiece = chessboard.getPieceAt(new Position(1,6));
        Piece blackKing = chessboard.getPieceAt(4,7);
        set.remove(blackPiece);
        set.remove(blackKing); // king can't be removed otherwise the check around it at the end will fail
        set.forEach(p -> chessboard.removePiece(p, false));
        // move black king otherwise the pawn can't be promoted because it becomes in check when the white pawn is promoted
        chessboard.move(blackKing.getPosition(), blackKing.getPosition().newPosition(0, -1));

        int size = chessboard.getActivePieces().get(Color.WHITE).size();
        int removed = chessboard.getRemovedPieces().get(Color.WHITE).size();

        Position finalBlackPosition = null;
        Position finalWhitePosition = null;
        for (int i = 0; i < 6; i++) {
            Position finalPosition = new Position(whitePiece.getPosition());
            finalPosition.moveBy(0, 1);
            chessboard.move(whitePiece.getPosition(), finalPosition);
            finalWhitePosition = finalPosition;

            finalPosition = new Position(blackPiece.getPosition());
            finalPosition.moveBy(0, -1);
            chessboard.move(blackPiece.getPosition(), finalPosition);
            finalBlackPosition = finalPosition;
        }

        Piece blackQueen = chessboard.getPieceAt(finalBlackPosition);
        assertNotNull(blackQueen);
        assertTrue(blackQueen instanceof Queen);
        Piece whiteQueen = chessboard.getPieceAt(finalWhitePosition);
        assertNotNull(whiteQueen);
        assertTrue(whiteQueen instanceof Queen);

        assertEquals(size, chessboard.getActivePieces().get(Color.WHITE).size());
        assertEquals(removed + 1, chessboard.getRemovedPieces().get(Color.WHITE).size());
    }

    @Test
    public void testBackwardMove() throws Exception {
        Chessboard chessboard = new Chessboard();
        chessboard.move(new Position(6,1), new Position(6,3)); // W
        chessboard.move(new Position(4,6), new Position(4,4)); // B
        try {
            chessboard.move(new Position(6, 3), new Position(6, 1)); // W
            fail("exception expected");
        } catch (InvalidaMoveException ex) {
            assertEquals("the Pawn can't move backwards", ex.getMessage());
        }
    }
}
