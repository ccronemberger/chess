package constantino.chess;

import constantino.chess.exceptions.CheckmateException;
import constantino.chess.pieces.Piece;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * This test will check 3 examples of checkmate as described in the examples section in:
 * https://en.wikipedia.org/wiki/Checkmate
 */
public class CheckmateTest {

    @Test
    public void foolsMate() throws Exception {
        Chessboard chessboard = new Chessboard();
        chessboard.move(new Position(6, 1), new Position(6, 3)); // W
        chessboard.move(new Position(4, 6), new Position(4, 4)); // B
        chessboard.move(new Position(5, 1), new Position(5, 2)); // W
        try {
            chessboard.move(new Position(3, 7), new Position(7, 3)); // B
            fail("exception expected");
        } catch (CheckmateException ex) {
            assertEquals("Checkmate, the BLACK won", ex.getMessage());
        }
    }

    @Test
    public void checkmateWithARook() throws Exception {
        Chessboard chessboard = new Chessboard();
        chessboard.state.removeAllBut(Color.WHITE, new Position(7, 0));
        chessboard.state.removeAllBut(Color.BLACK);
        Piece blackKing = chessboard.state.getPieceAt(4, 7);
        Piece whiteKing = chessboard.state.getPieceAt(4, 0);
        chessboard.state.setPieceAt(null, blackKing.getPosition());
        chessboard.state.setPieceAt(blackKing, new Position(7, 4));
        chessboard.state.setPieceAt(null, whiteKing.getPosition());
        chessboard.state.setPieceAt(whiteKing, new Position(4, 4));
        try {
            chessboard.move(whiteKing.getPosition(), whiteKing.getPosition().newPosition(1, 0));
            fail("exception expected");
        } catch (CheckmateException ex) {
            assertEquals("Checkmate, the WHITE won", ex.getMessage());
        }
    }

    @Test
    public void DByrneVsFischerTest() throws Exception {
        Chessboard chessboard = new Chessboard();
        chessboard.state.removeAllBut(Color.WHITE, new Position(3, 0), new Position(6, 1),
                new Position(7, 1), new Position(6, 0));
        Piece whitePawn1 = chessboard.state.getPieceAt(7, 1);
        chessboard.move(whitePawn1.getPosition(), whitePawn1.getPosition().newPosition(0, 2));

        Piece whiteQueen = chessboard.state.getPieceAt(3, 0);
        Piece whiteKnight = chessboard.state.getPieceAt(6, 0);
        Piece whiteKing = chessboard.state.getPieceAt(4, 0);
        moveDirectly(chessboard, whiteKing, new Position(2, 0));
        moveDirectly(chessboard, whiteKnight, new Position(4, 4));

        chessboard.state.removeAllBut(Color.BLACK, new Position(5, 6), new Position(6, 6),
                new Position(7, 6), new Position(1, 6), new Position(2, 6),
                new Position(2, 7), new Position(5, 7), new Position(1, 7),
                new Position(0, 7));
        Piece blackNight = chessboard.state.getPieceAt(1, 7);
        moveDirectly(chessboard, new Position(7, 6), new Position(7, 4));
        moveDirectly(chessboard, new Position(1, 6), new Position(1, 4));
        moveDirectly(chessboard, new Position(6, 6), new Position(6, 5));
        moveDirectly(chessboard, new Position(0, 7), new Position(2, 1));
        moveDirectly(chessboard, new Position(2, 7), new Position(1, 3));
        moveDirectly(chessboard, new Position(5, 7), new Position(1, 2));
        moveDirectly(chessboard, new Position(4, 7), new Position(6, 6));
        moveDirectly(chessboard, blackNight, new Position(2, 2));

        moveDirectly(chessboard, whiteQueen, new Position(1, 7));

        try {
            chessboard.move(new Position(2, 6), new Position(2, 5));
            fail("exception expected");
        } catch (CheckmateException ex) {
            assertEquals("Checkmate, the BLACK won", ex.getMessage());
        }
    }

    private void moveDirectly(Chessboard chessboard, Position position1, Position position2) {
        Piece piece = chessboard.state.getPieceAt(position1);
        chessboard.state.setPieceAt(null, piece.getPosition());
        chessboard.state.setPieceAt(piece, position2);
    }

    private void moveDirectly(Chessboard chessboard, Piece piece, Position position) {
        chessboard.state.setPieceAt(null, piece.getPosition());
        chessboard.state.setPieceAt(piece, position);
    }
}
