package constantino.chess;

import constantino.chess.exceptions.InvalidaMoveException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class ChessboardTest {

    @Test
    public void testInvalidPosition() {
        Chessboard chessboard = new Chessboard();
        try {
            chessboard.move(new Position(0, 2), new Position(0, 3));
            fail("exception expected");
        } catch (InvalidaMoveException ex) {
            assertEquals("there is no piece in position Position(x=0, y=2)", ex.getMessage());
        }
    }

    @Test
    public void twoConsecutiveMoves() throws Exception {
        Chessboard chessboard = new Chessboard();
        chessboard.move(new Position(3, 1), new Position(3, 3));

        try {
            chessboard.move(new Position(3, 3), new Position(3, 4));
            fail("exception expected");
        } catch (InvalidaMoveException ex) {
            assertEquals("the last move was already made by WHITE", ex.getMessage());
        }
    }

    @Test
    public void testKingWillBecomeEndangered() throws InvalidaMoveException {
        Chessboard chessboard = new Chessboard();
        Position queenPosition = new Position(3, 0);
        chessboard.state.removeAllBut(Color.WHITE, queenPosition);
        Position newQueenPosition = queenPosition.newPosition(1,1);
        chessboard.move(queenPosition, newQueenPosition);

        Position blackQueenPosition = new Position(3, 7);
        chessboard.state.removeAllBut(Color.BLACK, blackQueenPosition);
        chessboard.move(blackQueenPosition, blackQueenPosition.newPosition(1, -1));

        String stateStr = chessboard.state.toString();
        queenPosition = newQueenPosition;
        newQueenPosition = queenPosition.newPosition(-3, 3);
        moveAssertThrows(chessboard, queenPosition, newQueenPosition,
                "This move can't be performed otherwise the WHITE king would be or already is endangered");

        // the state can't change
        assertEquals(stateStr, chessboard.state.toString());
    }

    @Test
    public void testKingIsAlreadyEndangered() throws InvalidaMoveException {
        Chessboard chessboard = new Chessboard();
        Position queenPosition = new Position(3, 0);
        chessboard.state.removeAllBut(Color.WHITE, queenPosition);
        Position newQueenPosition = queenPosition.newPosition(1,1);
        chessboard.move(queenPosition, newQueenPosition);

        Position blackQueenPosition = new Position(3, 7);
        chessboard.state.removeAllBut(Color.BLACK, blackQueenPosition);

        moveAssertThrows(chessboard, blackQueenPosition, blackQueenPosition.newPosition(0, -7),
                "This move can't be performed otherwise the BLACK king would be or already is endangered");
    }

    @Test
    public void testCastlingWithIntermediatePositionEndangered() throws InvalidaMoveException {
        Chessboard chessboard = new Chessboard();
        Position queenPosition = new Position(3, 0);
        chessboard.state.removeAllBut(Color.WHITE, queenPosition);
        Position newQueenPosition = queenPosition.newPosition(0,1);
        chessboard.move(queenPosition, newQueenPosition);


        Position blackRookPosition = new Position(0, 7);
        chessboard.state.removeAllBut(Color.BLACK, blackRookPosition);

        moveAssertThrows(chessboard, new Position(4,7), new Position(2, 7),
                "Castling can't be performed because the path is under attack");
    }

    private void moveAssertThrows(Chessboard chessboard, Position initialPosition,
                                  Position finalPosition, String message) {
        try {
            chessboard.move(initialPosition, finalPosition);
            fail("exception is expected");
        } catch (InvalidaMoveException ex) {
            assertEquals(message, ex.getMessage());
        }
    }
}